#!/bin/bash
source /opt/ros/kinetic/setup.bash
cd ~/catkin_ws/src/imu_vn_100
source ~/catkin_ws/devel/setup.bash
roslaunch imu_vn_100 vn_100_cont.launch &
cd ~/catkin_ws/src/spinnaker_sdk_camera_driver
source ~/catkin_ws/devel/setup.bash
roslaunch spinnaker_sdk_camera_driver acquisition.launch &
cd ~/catkin_ws/src/VINS-Mono
source ~/catkin_ws/devel/setup.bash
roslaunch vins_estimator euroc.launch &
roslaunch vins_estimator vins_rviz.launch

