# CMake generated Testfile for 
# Source directory: /home/hayashi/catkin_ws/src
# Build directory: /home/hayashi/catkin_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(beginner_tutorials)
subdirs(VINS-Mono/benchmark_publisher)
subdirs(VINS-Mono/camera_model)
subdirs(VINS-Mono/ar_demo)
subdirs(VINS-Mono/feature_tracker)
subdirs(VINS-Mono/pose_graph)
subdirs(imu_vn_100)
subdirs(spinnaker_sdk_camera_driver)
subdirs(VINS-Mono/vins_estimator)
