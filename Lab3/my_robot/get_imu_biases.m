load('stationary_td2.mat')
stationary_data = data
time_0 = stationary_data(1,1);
time = (stationary_data(:,1)-time_0)/1000;
figure(1)
subplot(2,3,1)
acc_x = stationary_data(:,8);
b_a_x = mean(acc_x)
plot(time,acc_x,'.r')
hold on
plot(time,b_a_x*ones(1,length(time)),'-g')
v_a_x = var(acc_x)

subplot(2,3,2)
acc_y = stationary_data(:,9);
plot(time,acc_y,'.r')
hold on
b_a_y = mean(acc_y)
plot(time,b_a_y*ones(1,length(time)),'-g')
v_a_y = var(acc_y)

subplot(2,3,3)
acc_z = stationary_data(:,10);
plot(time,acc_z,'.r')
hold on
b_a_z = mean(acc_z)
plot(time,b_a_z*ones(1,length(time)),'-g')
v_a_z = var(acc_z)

subplot(2,3,4)
gyr_x= stationary_data(:,11);
plot(time,gyr_x,'.r')
b_g_x = mean(gyr_x)
hold on
plot(time,b_g_x*ones(1,length(time)),'-g')
v_g_x = var(gyr_x)

subplot(2,3,5)
gyr_y= stationary_data(:,12);
plot(time,gyr_y,'.r')
b_g_y = mean(gyr_y)
hold on
plot(time,b_g_x*ones(1,length(time)),'-g')
v_g_y = var(gyr_y)

subplot(2,3,6)
gyr_z = stationary_data(:,13);
plot(time,gyr_z,'.r')
b_g_z = mean(gyr_z)
hold on
plot(time,b_g_x*ones(1,length(time)),'-g');
v_g_z = var(gyr_z)

figure(2)
subplot(2,1,1)
angle_y = stationary_data(:,3);
b_angle_roll = mean(angle_y);
v_angle_roll = var(angle_y);
plot(time,angle_y,'.r')
hold on
plot(time,b_angle_roll*ones(1,length(time)),'-g')
grid on

subplot(2,1,2)
angle_z = stationary_data(:,2);
b_angle_yaw = mean(angle_z);
v_angle_yaw = var(angle_z);
plot(time,angle_z,'.r')
hold on
plot(time,b_angle_yaw*ones(1,length(time)),'-g')