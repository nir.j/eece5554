% Yaw angle 
load('mag_correction_param.mat')
load('imu_data_td5.mat')
imu_data = data;
imurate = 40.0
deg2rad_factor = pi/180;
imu_time_0 = imu_data((start*imurate)+1,1);
mag_data = imu_data(start*imurate+1:end,5:7);
imu_time = imu_data((start*imurate)+1:end,1) - imu_time_0; 

angle_z = wrapTo180(lowpass(imu_data(:,2),1,imurate)-imu_data(1,2)) ;
angle_rate_z = imu_data(:,13)/deg2rad_factor;
angle_z_i = (highpass(cumtrapz(1/imurate,angle_rate_z),.1,40));

x =  sigma*((mag_data(1:end,1)-center_x)*cos(theta) + (mag_data(1:end,2)-center_y)*sin(theta));
y = (-(mag_data(1:end,1)-center_x)*sin(theta) + (mag_data(1:end,2)-center_y)*cos(theta));
x_filt = lowpass(x,1,40);
y_filt = lowpass(y,1,40);
alpha = .9;
angle_z_m = wrapTo180((atan2(y_filt,x_filt)-atan2(y_filt(1),x_filt(1)))/deg2rad_factor);
%angle_z_m = wrapTo180((atan2(-y,x)-atan2(-y(1),x(1)))/deg2rad_factor);
%angle_z_m_filt =lowpass(angle_z_m,.1,40)
complementary = alpha*angle_z_m + (1-alpha)*angle_z_i;
figure(5)
plot(imu_time,angle_z_i,'b')
hold on
plot(imu_time,(complementary),'g')
grid on
grid minor
axis tight
plot(imu_time,angle_z_m,'-r')
 
omega = deg2rad(highpass(angle_rate_z,.1,40));
y_dd_e = (omega((start*imurate)+1:last*imurate).*v_x_i((start*imurate)+1:last*imurate));
figure(6)
hold on
subplot(3,1,1)
plot(imu_time((start*imurate)+1:last*imurate),y_dd_e,'r')
subplot(3,1,2)
plot(imu_time((start*imurate)+1:last*imurate),acc_y_filt((start*imurate)+1:last*imurate),'b')
axis tight
subplot(3,1,3)
plot(imu_time((start*imurate)+1:last*imurate),acc_pitch((start*imurate)+1:last*imurate),'b')
axis tight
