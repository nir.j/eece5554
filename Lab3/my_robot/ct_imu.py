#!/usr/bin/env python
# -*- coding: utf-8 -*-
# for GPS sensor - Sensing and Navigation - EECE 5554

import sys
import lcm
import time
import serial
import gps_imu_lcm
import numpy as np

class IMU(object):
    def __init__(self, port_name):
        self.port = serial.Serial(port_name, 115200, timeout=1.)  # 9600-N-8-1
	
	#initlialize LCM with network port ; network port can be null     
	self.lcm = lcm.LCM() 
        self.imu_packet = gps_imu_lcm.imu_t()
	print('CT_initialization done')

    def readloop(self):
        while True:
            line = self.port.readline()
	    #print(line)
	 #   print('\n')
	    strings = line.split(",")
	    if strings[0] == "$VNYMR":
				
		# Assign values to packet structure
		self.imu_packet.timestamp = time.time()  # micro sec passed from 1970/1/1
		print(self.imu_packet.timestamp)
		self.imu_packet.theta_z = float(strings[1])
		self.imu_packet.theta_y = float(strings[2])
		self.imu_packet.theta_x = float(strings[3])
		self.imu_packet.mag_x = float(strings[4])
		self.imu_packet.mag_y = float(strings[5])
		self.imu_packet.mag_z = float(strings[6])
		self.imu_packet.acc_x = float(strings[7])
		self.imu_packet.acc_y = float(strings[8])
		self.imu_packet.acc_z = float(strings[9])
		self.imu_packet.theta_x_d = float(strings[10])
		self.imu_packet.theta_y_d = float(strings[11])
		imu_str = np.array(strings[12].split('*'))
		self.imu_packet.theta_z_d = float(imu_str[0])
		
		# publish the message on CT_GPS channel 
                self.lcm.publish("CT_IMU", self.imu_packet.encode())

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: %s <serial_port>\n" % sys.argv[0]
        sys.exit(0)
    imu_s = IMU(sys.argv[1])
    imu_s.readloop()
