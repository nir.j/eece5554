% add the lcm.jar file to the matlabpath - need to only do this once
javaaddpath /usr/local/share/java/lcm.jar
javaaddpath lcm-spy/gps_imu.jar

% Let’s assume the logging file is lcm-l.02 in the dir below
% open log file for reading

log_file = lcm.logging.Log('stationary', 'r'); 

% now read the file 
% here we are assuming that the channel we are interested in is RDI. Your channel 
% name will be different - something to do with GPS
% also RDI has fields altitude and ranges - GPS will probably have lat, lon, utmx,
% utmy etc
lat = zeros(1000,1);lon = zeros(1000,1);
utm_x = zeros(1000,1);utm_y = zeros(1000,1);
alt = zeros(1000,1);time = zeros(1000,1);
data = zeros(600,13);
i = 1;
while true
 try
   ev = log_file.readNext();
   
   % channel name is in ev.channel
   % there may be multiple channels but in this case you are only interested in RDI channel
   if strcmp(ev.channel, 'CT_GPS')
 
     % build rdi object from data in this record
      rdi = imu_lcm.imu_t(ev.data);
      % now you can do things like depending upon the rdi_t struct that was defined
      data(i,1) = rdi.timestamp;
      data(i,2) = rdi.theta_z;
      data(i,3) = rdi.theta_y;
      data(i,4) = rdi.theta_x;
      data(i,5) = rdi.mag_x;
      data(i,6) = rdi.mag_y;
      data(i,7) = rdi.mag_z;
      data(i,8) = rdi.acc_x;
      data(i,9) = rdi.acc_y;
      data(i,10) = rdi.acc_z;
      data(i,11) = rdi.theta_x_d;
      data(i,12) = rdi.theta_y_d;
      data(i,13) = rdi.theta_z_d;
      i = i+1;
    end
  catch err   % exception will be thrown when you hit end of file
     break;
  end
end

