clear all
close all
hold on;
format longG
load('magnetometer_gps_data.mat');
offset_x = data(1,2)
offset_y = data(1,3)
start = 1
range = 300
plot(data(start,5),data(start,6),'Or') % start
plot(data(range,5),data(range,6),'Og') % last point
plot(data(start:range,5),data(start:range,6),'--b')
axis('equal')
xlabel('UTM-X');
ylabel('UTM-Y');
title(sprintf('Origin: x  = %f, y = %f in m',offset_x,offset_y))