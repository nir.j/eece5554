clear all;
close all;
load('gps_data_td5.mat')
gps_data = data;
load('imu_biases_td2.mat')
load('imu_data_td5.mat')
g = 9.81;
deg2rad_factor = pi/180;
b_a_z = g + b_a_z;
imu_data = data;
imurate = 40.0
start = 0 
last = 600
b_angle_roll = 1.68;
b_angle_yaw = -158.25;
imu_time_0 = imu_data((start*imurate)+1,1);
imu_time = imu_data((start*imurate)+1:end,1) - imu_time_0; 
acc_x = imu_data((start*imurate)+1:end,8) - b_a_x;
acc_y = imu_data((start*imurate)+1:end,9) - b_a_y;
acc_z = imu_data((start*imurate)+1:end,10) - b_a_z;

acc_mag = sqrt(acc_x.^2 + acc_y.^2 + acc_z.^2);
acc_x_filt = lowpass(acc_x,.025,40);
acc_y_filt = bandpass(acc_y,[.1 1],40);
acc_z_filt = movmean(acc_z,3);

angle_x = lowpass(imu_data(:,4),1,imurate) 
angle_y = lowpass(imu_data(:,3),.5,imurate) - b_angle_roll ;
angle_z = lowpass(imu_data(:,2),1,imurate) ;
angle_rate_y = lowpass(imu_data(:,12)/deg2rad_factor,16,40);

halt_condition = ~(acc_mag <= 1.01*g & acc_mag >= 0.999*g); % vehicle was at rest here because net acceleration is nearly g
angle_y_i = cumtrapz(1/imurate,halt_condition.*angle_rate_y)

acc_pitch = g*angle_x*deg2rad_factor;
acc_roll = -g*angle_y_i*deg2rad_factor; % for positive roll the acceleration is negative x in imu considered small roll angle
acc_x_corrected = lowpass(halt_condition.*(acc_x_filt - acc_roll),10,40);
v_x_i = cumtrapz(1/imurate,acc_x_corrected((start*imurate)+1:last*imurate));
v_x_i_u = cumtrapz(1/imurate,acc_x_filt((start*imurate)+1:last*imurate));

offset_x = gps_data(start+1,5) - mod(gps_data(start+1,5),10);
offset_y = gps_data(start+1,6) - mod(gps_data(start+1,6),10);
range = (start+1):last;
gps_time_0 = gps_data(start+1,1);
gps_time = (gps_data(range,1)-gps_time_0)/1000000.0;

displacement_x = gps_data(range,5)-offset_x;
displacement_y = gps_data(range,6)-offset_y;
v_x = diff(displacement_x);
v_y = diff(displacement_y);

speed = sqrt(v_x.^2 + v_y.^2);
speed_lp = lowpass(speed,.4,1);

% Compare x_dot

%% Plots
figure(1)
hold on
subplot(3,1,1)
plot(imu_time((start*imurate)+1:last*imurate),acc_x_corrected((start*imurate)+1:last*imurate),'b')
grid minor
subplot(3,1,2)
plot(imu_time((start*imurate)+1:last*imurate),acc_mag((start*imurate)+1:last*imurate)-g)
grid on
subplot(3,1,3)
grid on
hold on
plot(imu_time((start*imurate)+1:last*imurate),angle_y_i((start*imurate)+1:last*imurate),'b')

figure(2)
plot(imu_time((start*imurate)+1:(last)*imurate),v_x_i,'b')
grid on
hold on
plot(gps_time(1:end),[0;speed_lp],'r')
plot(imu_time((start*imurate)+1:(last)*imurate),v_x_i_u,'g')
grid minor

figure(3)
for i = 1:last
    if mod(i,50) == 0 
        plot(gps_data(i,5)-offset_x,gps_data(i,6)-offset_y,'Or');
        text(gps_data(i,5)-offset_x-10,gps_data(i,6)-offset_y+20,num2str(i));
    else
        plot(gps_data(i,5)-offset_x,gps_data(i,6)-offset_y,'*b');
    end
    hold on;
end

xlabel('UTM-X');
ylabel('UTM-Y');
grid on
title(sprintf('Origin: x  = %f, y = %f in m',offset_x,offset_y)) 
compute_yaw_angle;
 
figure(4)
plot(gps_data(:,2),gps_data(:,3),'.')

