clear all
close all
load('magnetometer_imu_data.mat')
start = 30
last = 90
imurate = 40.00
mag_data = data(start*imurate+1:last*imurate,5:7);

% Compute radius vector 
center_x = mean(mag_data(1:end,1))
center_y = mean(mag_data(1:end,2))
r = sqrt((mag_data(1:end,1)-center_x).^2 + (mag_data(1:end,2)-center_y).^2);
r_filtered = lowpass(r,.1,40); % filter data with cut off frequency of .1 hz

figure(1)
h = axes
axis equal
grid on
hold on;
plot(mag_data(1:end,1),mag_data(1:end,2),'.b') % y vs x
ellipse_t = fit_ellipse(mag_data(:,1),mag_data(:,2),h);

% subplot(3,1,2)
% plot(mag_data(1:end,2),mag_data(1:end,3),'b*') % z vs y
% axis equal
% 
% subplot(3,1,3)
% plot(mag_data(1:end,1),mag_data(1:end,3),'*') % z vs x
% hold on;

figure(2)
plot(r_filtered)
hold on
% Get maximum radius vector
offset = 0;
[r_max,loc_max] = max(r_filtered(offset+1:end)) % major axis length
[r_min,loc_min] = min(r_filtered(offset+1:end)) % minor axis length
theta = atan2(mag_data(loc_max+offset,2)-center_y,mag_data(loc_max+offset,1)-center_x)

figure(1)
hold on
plot(mag_data(12,1),mag_data(12,2),'*g')
plot(mag_data(loc_min+offset,1),mag_data(loc_min+offset,2),'*b')
plot(center_x,center_y,'*c')
axis tight

theta = pi/2-ellipse_t.phi;
sigma = ellipse_t.short_axis/ellipse_t.long_axis;
center_x = ellipse_t.X0_in;
center_y = ellipse_t.Y0_in;
x =  sigma*((mag_data(1:end,1)-center_x)*cos(theta) + (mag_data(1:end,2)-center_y)*sin(theta));
y = (-(mag_data(1:end,1)-center_x)*sin(theta) + (mag_data(1:end,2)-center_y)*cos(theta));

figure(3)
plot(x,y,'.')
grid on
grid minor
axis equal
axis tight

figure(4)
nrm = sqrt(x.^2 + (y).^2 + (sigma*mag_data(:,3).^2))
plot(nrm)

ellipse_t
