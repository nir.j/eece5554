% Intrinsic and Extrinsic Camera Parameters
%
% This script file can be directly executed under Matlab to recover the camera intrinsic and extrinsic parameters.
% IMPORTANT: This file contains neither the structure of the calibration objects nor the image coordinates of the calibration points.
%            All those complementary variables are saved in the complete matlab data file Calib_Results.mat.
% For more information regarding the calibration model visit http://www.vision.caltech.edu/bouguetj/calib_doc/


%-- Focal length:
fc = [ 3472.388742248433573 ; 3481.664593609136773 ];

%-- Principal point:
cc = [ 1730.220177425130487 ; 2306.348166253139880 ];

%-- Skew coefficient:
alpha_c = 0.000000000000000;

%-- Distortion coefficients:
kc = [ 0.122752209799188 ; -0.294938652687068 ; 0.002692968866631 ; 0.000015612733734 ; 0.000000000000000 ];

%-- Focal length uncertainty:
fc_error = [ 3.198744533660686 ; 3.117998509307942 ];

%-- Principal point uncertainty:
cc_error = [ 1.650578039705189 ; 1.455926885286846 ];

%-- Skew coefficient uncertainty:
alpha_c_error = 0.000000000000000;

%-- Distortion coefficients uncertainty:
kc_error = [ 0.001576740799662 ; 0.005675721902809 ; 0.000164563769693 ; 0.000175862908052 ; 0.000000000000000 ];

%-- Image size:
nx = 3456;
ny = 4608;


%-- Various other variables (may be ignored if you do not use the Matlab Calibration Toolbox):
%-- Those variables are used to control which intrinsic parameters should be optimized

n_ima = 20;						% Number of calibration images
est_fc = [ 1 ; 1 ];					% Estimation indicator of the two focal variables
est_aspect_ratio = 1;				% Estimation indicator of the aspect ratio fc(2)/fc(1)
center_optim = 1;					% Estimation indicator of the principal point
est_alpha = 0;						% Estimation indicator of the skew coefficient
est_dist = [ 1 ; 1 ; 1 ; 1 ; 0 ];	% Estimation indicator of the distortion coefficients


%-- Extrinsic parameters:
%-- The rotation (omc_kk) and the translation (Tc_kk) vectors for every calibration image and their uncertainties

%-- Image #1:
omc_1 = [ -2.174051e+00 ; -2.182941e+00 ; 5.331715e-02 ];
Tc_1  = [ -8.453496e+01 ; -5.306566e+01 ; 2.372467e+02 ];
omc_error_1 = [ 4.455585e-04 ; 4.607110e-04 ; 9.555785e-04 ];
Tc_error_1  = [ 1.130852e-01 ; 1.010589e-01 ; 2.245266e-01 ];

%-- Image #2:
omc_2 = [ -2.120721e+00 ; -2.078946e+00 ; 1.995053e-01 ];
Tc_2  = [ -8.970909e+01 ; -4.993710e+01 ; 2.363137e+02 ];
omc_error_2 = [ 4.237356e-04 ; 4.177438e-04 ; 8.321422e-04 ];
Tc_error_2  = [ 1.112084e-01 ; 1.000296e-01 ; 2.149130e-01 ];

%-- Image #3:
omc_3 = [ 2.109642e+00 ; 2.076334e+00 ; 2.950527e-01 ];
Tc_3  = [ -6.387001e+01 ; -5.212749e+01 ; 1.962345e+02 ];
omc_error_3 = [ 4.291443e-04 ; 3.724033e-04 ; 8.266799e-04 ];
Tc_error_3  = [ 9.639203e-02 ; 8.403819e-02 ; 1.840281e-01 ];

%-- Image #4:
omc_4 = [ 2.126402e+00 ; 2.134640e+00 ; 5.670942e-01 ];
Tc_4  = [ -6.752679e+01 ; -5.858974e+01 ; 1.891532e+02 ];
omc_error_4 = [ 4.410475e-04 ; 3.571852e-04 ; 8.389824e-04 ];
Tc_error_4  = [ 9.499867e-02 ; 8.264190e-02 ; 1.874017e-01 ];

%-- Image #5:
omc_5 = [ 2.188255e+00 ; 2.084498e+00 ; -2.153446e-01 ];
Tc_5  = [ -8.376219e+01 ; -3.001844e+01 ; 2.412259e+02 ];
omc_error_5 = [ 4.682405e-04 ; 4.413524e-04 ; 9.514743e-04 ];
Tc_error_5  = [ 1.141193e-01 ; 1.016184e-01 ; 2.232920e-01 ];

%-- Image #6:
omc_6 = [ -1.721876e+00 ; -2.135496e+00 ; 4.546256e-01 ];
Tc_6  = [ -7.304914e+01 ; -4.176906e+01 ; 2.898081e+02 ];
omc_error_6 = [ 4.076492e-04 ; 4.894497e-04 ; 7.278002e-04 ];
Tc_error_6  = [ 1.353765e-01 ; 1.219810e-01 ; 2.288207e-01 ];

%-- Image #7:
omc_7 = [ NaN ; NaN ; NaN ];
Tc_7  = [ NaN ; NaN ; NaN ];
omc_error_7 = [ NaN ; NaN ; NaN ];
Tc_error_7  = [ NaN ; NaN ; NaN ];

%-- Image #8:
omc_8 = [ NaN ; NaN ; NaN ];
Tc_8  = [ NaN ; NaN ; NaN ];
omc_error_8 = [ NaN ; NaN ; NaN ];
Tc_error_8  = [ NaN ; NaN ; NaN ];

%-- Image #9:
omc_9 = [ 2.159160e+00 ; 2.091808e+00 ; -8.157896e-02 ];
Tc_9  = [ -8.188964e+01 ; -5.652375e+01 ; 2.249865e+02 ];
omc_error_9 = [ 4.174348e-04 ; 4.314390e-04 ; 8.798631e-04 ];
Tc_error_9  = [ 1.080426e-01 ; 9.510832e-02 ; 2.089457e-01 ];

%-- Image #10:
omc_10 = [ NaN ; NaN ; NaN ];
Tc_10  = [ NaN ; NaN ; NaN ];
omc_error_10 = [ NaN ; NaN ; NaN ];
Tc_error_10  = [ NaN ; NaN ; NaN ];

%-- Image #11:
omc_11 = [ -2.071726e+00 ; -2.106959e+00 ; 1.811897e-01 ];
Tc_11  = [ -9.784769e+01 ; -5.237153e+01 ; 2.519092e+02 ];
omc_error_11 = [ 4.429439e-04 ; 4.426609e-04 ; 8.677100e-04 ];
Tc_error_11  = [ 1.185580e-01 ; 1.072188e-01 ; 2.302085e-01 ];

%-- Image #12:
omc_12 = [ 2.005707e+00 ; 2.023945e+00 ; 3.618305e-01 ];
Tc_12  = [ -6.186052e+01 ; -5.144403e+01 ; 2.047234e+02 ];
omc_error_12 = [ 4.305573e-04 ; 3.845546e-04 ; 8.015153e-04 ];
Tc_error_12  = [ 1.004758e-01 ; 8.731720e-02 ; 1.920239e-01 ];

%-- Image #13:
omc_13 = [ NaN ; NaN ; NaN ];
Tc_13  = [ NaN ; NaN ; NaN ];
omc_error_13 = [ NaN ; NaN ; NaN ];
Tc_error_13  = [ NaN ; NaN ; NaN ];

%-- Image #14:
omc_14 = [ NaN ; NaN ; NaN ];
Tc_14  = [ NaN ; NaN ; NaN ];
omc_error_14 = [ NaN ; NaN ; NaN ];
Tc_error_14  = [ NaN ; NaN ; NaN ];

%-- Image #15:
omc_15 = [ -2.060645e+00 ; -1.921726e+00 ; 2.328310e-01 ];
Tc_15  = [ -1.023685e+02 ; -2.820898e+01 ; 2.632118e+02 ];
omc_error_15 = [ 4.353615e-04 ; 4.308058e-04 ; 7.635868e-04 ];
Tc_error_15  = [ 1.232520e-01 ; 1.120368e-01 ; 2.290954e-01 ];

%-- Image #16:
omc_16 = [ -2.169605e+00 ; -2.144463e+00 ; 5.494849e-02 ];
Tc_16  = [ -9.317026e+01 ; -2.757369e+01 ; 2.466468e+02 ];
omc_error_16 = [ 4.437484e-04 ; 4.952772e-04 ; 9.423071e-04 ];
Tc_error_16  = [ 1.165369e-01 ; 1.054713e-01 ; 2.342446e-01 ];

%-- Image #17:
omc_17 = [ -2.141475e+00 ; -2.119558e+00 ; 1.183866e-01 ];
Tc_17  = [ -7.977644e+01 ; -2.784981e+01 ; 2.541478e+02 ];
omc_error_17 = [ 4.347985e-04 ; 5.061995e-04 ; 9.452695e-04 ];
Tc_error_17  = [ 1.197194e-01 ; 1.074661e-01 ; 2.360221e-01 ];

%-- Image #18:
omc_18 = [ -2.092016e+00 ; -2.077510e+00 ; 2.478259e-01 ];
Tc_18  = [ -8.664903e+01 ; -2.656502e+01 ; 2.759951e+02 ];
omc_error_18 = [ 4.534353e-04 ; 5.013878e-04 ; 9.144031e-04 ];
Tc_error_18  = [ 1.291070e-01 ; 1.163978e-01 ; 2.452722e-01 ];

%-- Image #19:
omc_19 = [ 2.148992e+00 ; 2.137852e+00 ; 1.820408e-01 ];
Tc_19  = [ -6.644536e+01 ; -2.797011e+01 ; 2.337531e+02 ];
omc_error_19 = [ 4.976563e-04 ; 4.058679e-04 ; 9.841103e-04 ];
Tc_error_19  = [ 1.127170e-01 ; 9.982773e-02 ; 2.212949e-01 ];

%-- Image #20:
omc_20 = [ 2.080144e+00 ; 2.064854e+00 ; -2.511180e-01 ];
Tc_20  = [ -7.836094e+01 ; -3.600509e+01 ; 2.366872e+02 ];
omc_error_20 = [ 4.231876e-04 ; 4.295099e-04 ; 8.689999e-04 ];
Tc_error_20  = [ 1.118857e-01 ; 9.924998e-02 ; 2.117781e-01 ];

