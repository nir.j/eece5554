% Read grayscale image
I = imread(filename);
I_g = rgb2gray(I);

%Get corners
[y,x,m] = harris(I_g,1000,'tile',[2,2],'disp');
