#!/usr/bin/env python
import rospy
from std_msgs.msg import String

def talker():
    pub = rospy.Publisher('SumParameters', String, queue_size=10)
    rospy.init_node('addParameter', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    a = rospy.get_param("a")
    b = rospy.get_param("b")
    while not rospy.is_shutdown():

        msg = "%s Adding parameters %s" % (rospy.get_time(), str(a+b))
        rospy.loginfo(msg)
        pub.publish(msg)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass
