#!/usr/bin/env python
import rospy
import sys
import serial, string, math, time, struct 
import socket #
import time
import utm
from sensor_msgs.msg import NavSatFix

def readloop(port,gpsPub,gpsData):
   rate = rospy.Rate(10)
   while not rospy.is_shutdown():
      line = port.readline()
      if line.startswith('$GNGGA'):
          gpsvals = line.strip().split(',')
      lat = (float(gpsvals[2])%100)/60 + float(gpsvals[2])//100
      if gpsvals[3] == 'S': 
          lat*=-1
      lon  = (float(gpsvals[4])%100)/60 + float(gpsvals[4])//100
      if gpsvals[5] == 'W':
          lon*=-1
      gpsData.latitude = float(lat)
      gpsData.longitude = float(lon)
      gpsData.altitude = float(gpsvals[9]
      gpsPub.publish(gpsData)
      rospy.loginfo("GPS Data %s", gpsData)
      rate.sleep()
                

if __name__ == "__main__":
    try:    
      	portName = rospy.get_param('port_name', '/dev/ttyACM0')
      	baudRate = rospy.get_param('baud_rate', '115200')
      	port = serial.Serial(port_name, BaudRate, timeout=1)
      	gpsPub = rospy.Publisher('rtk_fix', NavSatFix, queue_size=10)
      	gpsData = NavSatFix()
      	rospy.init_node('to_utm', anonymous=True)
      	readloop(1,gpsPub,gpsData)
    except rospy.ROSInterruptException:
      	pass
