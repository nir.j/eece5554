#!/usr/bin/env python
import rospy
import sys
import serial
import utm
import std_msgs.msg
from sensor_msgs.msg import NavSatFix
from nav_msgs.msg import Odometry
from geometry_msgs.msg import Pose, Point

def callback(data): 
     lat = float(data.latitude)        
     lon = float(data.longitude)
     alt = float(data.altitude)
     easting, northing, zonenum, zonelet = utm.from_latlon(lat, lon)
     x = float(easting)
     y = float(northing)
     z = float(alt) 
     utmData.pose.pose.position = Point(x,y,z) 
     utmPub.publish(utmData)
     rospy.loginfo("UTM Data %s", utmData)

def listener():
     rospy.init_node('gps', anonymous=True)
     rospy.Subscriber("rtk_fix", NavSatFix,callback)        
     rospy.spin()          

if __name__ == '__main__':
     try:                              
     	utmPub = rospy.Publisher('utm_fix', Odometry, queue_size=1)
     	utmData = Odometry()                                 
     	listener()            
     except rospy.ROSInterruptException:
        pass
