format longG

% Get bag object
bag = rosbag('ROS_bags/Stationary_Lot.bag')
bag.AvailableTopics

% rtk_fix messages
rtk_fix = select(bag,'Topic','/rtk_fix');
rtk_msg = readMessages(rtk_fix);

% utm_fix messages
utm_fix = select(bag,'Topic','/utm_fix');
utm_msg = readMessages(utm_fix);

% Extract data in matrices
n = length(rtk_msg);
m = length(utm_msg);
lat_long = zeros(n,3);
utm_coord = zeros(m,3);
for i = 1:n
    lat_long(i,1) = rtk_msg{i}.Latitude;
    lat_long(i,2) = rtk_msg{i}.Longitude;
    lat_long(i,3) = rtk_msg{i}.Altitude;
end 

for i = 1:m
    utm_coord(i,1) = utm_msg{i}.Pose.Pose.Position.X;
    utm_coord(i,2) = utm_msg{i}.Pose.Pose.Position.Y;
    utm_coord(i,3) = utm_msg{i}.Pose.Pose.Position.Z;
end 

offset_x = utm_coord(1,1);
offset_y = utm_coord(1,2);
offset_z = utm_coord(1,3)
plot3(utm_coord(:,1)-offset_x,utm_coord(:,2)-offset_y,utm_coord(:,3)-offset_z,'r*')
axis('equal')
xlim([-.010,.010])
ylim([-.010,.010])
zlim([-.010,.010])
grid on

    