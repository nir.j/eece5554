#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Created on Sun Feb 17 14:59:40 2019

@author: jagat
"""
import sys
import utm
import csv
from matplotlib import pyplot as plt
import numpy as np

coord = np.zeros([1000,2])
def ReadCSV(filename):
     with open(filename) as csvfile:
         csv_reader = csv.reader(csvfile, delimiter=',')
	 i = 0
         for row in csv_reader:
            lat = float(row[0])
	    lon = float(row[1])
	   # print(type(lat))
	   # print('lat = ' + str(lat) + ',lon = ' + str(lon))
	    utmCoord = utm.from_latlon(lat,lon)
	    coord[i,0] = utmCoord[0]
	    coord[i,1] = utmCoord[1]
	    i = i + 1
         plt.plot(coord[:,0]-coord[0,0],coord[:,1]-coord[0,1],'*r')
	 plt.axis('equal')
         plt.xlim([-1,1])
         plt.ylim([-2,2])
         plt.show()
if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: %s <serial_port>\n" % sys.argv[0]
        sys.exit(0)
    ReadCSV(sys.argv[1])
