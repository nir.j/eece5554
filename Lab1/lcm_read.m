% add the lcm.jar file to the matlabpath - need to only do this once
javaaddpath /usr/local/share/java/lcm.jar
javaaddpath lcm-spy/gps.jar

% Let’s assume the logging file is lcm-l.02 in the dir below
% open log file for reading

log_file = lcm.logging.Log('lcm-logs/lcmlog-2019-02-01.00', 'r'); 

% now read the file 
% here we are assuming that the channel we are interested in is RDI. Your channel 
% name will be different - something to do with GPS
% also RDI has fields altitude and ranges - GPS will probably have lat, lon, utmx,
% utmy etc
lat = zeros(1000,1);lon = zeros(1000,1);
utm_x = zeros(1000,1);utm_y = zeros(1000,1);
alt = zeros(1000,1);time = zeros(1000,1);
i = 1;
while true
 try
   ev = log_file.readNext();
   
   % channel name is in ev.channel
   % there may be multiple channels but in this case you are only interested in RDI channel
   if strcmp(ev.channel, 'CT_GPS')
 
     % build rdi object from data in this record
      rdi = gps_lcm.gps_t(ev.data);
      % now you can do things like depending upon the rdi_t struct that was defined
      lat(i) = rdi.lat;
      lon(i) = rdi.lon;
      utm_x(i) = rdi.utm_x;
      utm_y(i) = rdi.utm_y;
      alt(i) = rdi.alt;    
      time(i) = rdi.timestamp;  % (timestamp in microseconds since the epoch)
      i = i+1;
    end
  catch err   % exception will be thrown when you hit end of file
     break;
  end
end
data = [time,lat,lon,alt,utm_x,utm_y];
