hold on;
format longG
load('MAT_files/GPS_static_10min.mat');
offset_x = 673240
offset_y = 4688900
range = 300
offsetUTMData = [data(1:range,5)-offset_x, data(1:range,6)-offset_y];
mean_x = mean(data(1:range,5)-offset_x)
mean_y = mean(data(1:range,6)-offset_y)
plot(data(1,5) - offset_x,data(1,6) - offset_y,'or') % start
plot(data(300,5)- offset_x,data(300,6)- offset_y,'og') % last point
plot(mean_x,mean_y,'b--s')
plot(data(1:range,5)-offset_x,data(1:range,6)-offset_y,'-*b')
xlabel('UTM-X');
ylabel('UTM-Y');
title(sprintf('Origin: x  = %f, y = %f in m',offset_x,offset_y))
axis equal
std_x = std(data(1:range,5)-offset_x)
std_y = std(data(1:range,6)-offset_y)
r = sqrt(std_x^2+std_y^2);

hold on
th = 0:pi/50:2*pi;
xunit = r * cos(th) + mean_x;
yunit = r * sin(th) + mean_y;
h = plot(xunit, yunit,'.b');

xunit = 1.5*r * cos(th) + mean_x;
yunit = 1.5*r * sin(th) + mean_y;
h = plot(xunit, yunit,'.b');

xunit = 2*r * cos(th) + mean_x;
yunit = 2*r * sin(th) + mean_y;
h = plot(xunit, yunit,'.b');

