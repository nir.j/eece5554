clear all
hold on;
format longG
load('MAT_files/gpsData_movingStraight_ISEC.mat');
offset_x = 671980
offset_y = 4689260
range = 300
plot(data(1,5) - offset_x,data(1,6) - offset_y,'Or') % start
plot(data(300,5)- offset_x,data(300,6)- offset_y,'Og') % last point
plot(data(1:range,5)-offset_x,data(1:range,6)-offset_y,'--b')
xlabel('UTM-X');
ylabel('UTM-Y');
title(sprintf('Origin: x  = %f, y = %f in m',offset_x,offset_y))