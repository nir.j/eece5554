#!/usr/bin/env python
# -*- coding: utf-8 -*-
# for GPS sensor - Sensing and Navigation - EECE 5554

import sys
import lcm
import time
import serial
import gps_lcm
import utm

class GPS(object):
    def __init__(self, port_name):
        self.port = serial.Serial(port_name, 4800, timeout=1.)  # 9600-N-8-1
	
	#initlialize LCM with network port ; network port can be null     
	self.lcm = lcm.LCM() 
        self.packet = gps_lcm.gps_t()
	print('CT_initialization done')

    def readloop(self):
        while True:
            line = self.port.readline()
	#    print(line)
	 #   print('\n')
	    strings = line.split(",")
	    if strings[0] == "$GPGGA":
		#print(line)
		#Convert read gps values to degrees for utm use
		lat = self.toDegrees(float(strings[2]),'lat')
		lon = self.toDegrees(float(strings[4]),'lon')
		#print('lat =' + str( lat))
		#print('lon =' + str(lon))

		# Compute UTM coordinates
		utmCoord = utm.from_latlon(lat,lon)
		
		# Assign values to packet structure
		self.packet.timestamp = time.time() * 1e6 # micro sec passed from 1970/1/1
                self.packet.lat = lat
                self.packet.lon = lon
		self.packet.alt = float(strings[9])
		self.packet.utm_x = utmCoord[0]
		self.packet.utm_y = utmCoord[1]
		
		# publish the message on CT_GPS channel 
                self.lcm.publish("CT_GPS", self.packet.encode())
    
    def toDegrees(self,gpsRead,t):
	#print('inside toDegrees gps Read = ' + str(gpsRead) + ' type ='+t)
	if t == 'lat':
	   deg = int((gpsRead/100.0)) % 100
	   mnt = ((gpsRead*10000) % (deg*1000000.0))/10000
	elif t == 'lon':
	   deg = int((gpsRead/100.0)) % 1000
    	   mnt = ((gpsRead*10000) % (deg*1000000.0))/10000
	readingDeg = deg + mnt/60.0
	return readingDeg

if __name__ == "__main__":
    if len(sys.argv) != 2:
        print "Usage: %s <serial_port>\n" % sys.argv[0]
        sys.exit(0)
    gps_s = GPS(sys.argv[1])
    gps_s.readloop()
